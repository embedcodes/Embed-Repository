# Krypton
>https://watch-series.io/series/krypton-season-1-episode-1

>http://ww2.watch-series.co/

>http://watch-series.co/

>http://watch-series.ru/

>https://jsfiddle.net/ou4fodb4/

>https://mangarock.com/manga/mrs-serie-100246250

# Platform embed codes


`Script`
`HTML`
`CSS`
`3rd Party`

<!-- Links -->
* [Ecwid](#ecwid)



<!-- ECWID -->
<a name="ecwid"></a>
# Ecwid embed codes

<!-- Anchor Links -->
#### Links
* [Ecwid additional button](#01)
* [Ecwid additional link](#02)
* [Ecwid change text link](#03)
 



<!-- Contents -->

> #### Additional button below checkout button
<!-- additional Button-->
<a name="01"></a>

> Look for the STORE ID and update it.
> Here's the [Code](https://gitlab.com/embedcodes/Embed-Repository/blob/master/Ecwid/ecwid-cart-additional-button.html)

![Request Button](/Ecwid/output%20images/image-00001-ecwid-add-button.PNG)



> #### Additional link above add to cart button
<!-- additional link-->
<a name="02"></a>

> Here's the [Code](https://gitlab.com/embedcodes/Embed-Repository/blob/master/Ecwid/ecwid-cart-additional-button.html)

![Additional Link](/Ecwid/output%20images/image-00002-ecwid-add-link.PNG)


> #### Change text link
<!-- change text link-->
<a name="03"></a>

> Here's the [Code](https://gitlab.com/embedcodes/Embed-Repository/blob/master/Ecwid/ecwid-cart-additional-button.html)

![Change Text Link](/Ecwid/output%20images/image-00003-ecwid-change-link-title.PNG)



